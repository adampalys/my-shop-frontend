import { from, of } from 'rxjs';
import {
    debounceTime, switchMap, mergeMap, takeUntil, catchError,
} from 'rxjs/operators';
import { ofType } from 'redux-observable';
import { sendData } from '../service/ajax';
import sessionActions from '../actions/session';

const preparedData = data => ({
    Username: data.username,
    Password: data.password,
});

const authenticateEpic = (action$, store) => (
    action$.pipe(
        ofType('AUTHENTICATE'),
        debounceTime(250),
        switchMap(data => (
            from(sendData('/users/authenticate', preparedData(data)))
                .pipe(
                    takeUntil(action$.ofType('AUTHENTICATE')),
                    mergeMap((response) => {
                        window.localStorage.setItem('token', response.token);

                        return of(sessionActions.setUserData(response));
                    }),
                    catchError(() => of(sessionActions.showWarning())),
                )
        )),
    )
);

export default authenticateEpic;
