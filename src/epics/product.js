import { combineEpics } from 'redux-observable';
import { createEpics as createListEpics } from './abstr/listEpicFactory';
import { createEpics as createShowEpics } from './abstr/showEpicFactory';
import { createEpics as createFormEpics } from './abstr/formEpicFactory';
import {
    productListActions,
    productShowActions,
    productFormActions,
} from '../actions/product';

const productListEpics = createListEpics(productListActions);
const productShowEpics = createShowEpics(productShowActions);
const productFormEpics = createFormEpics(productFormActions);

export default combineEpics(
    productListEpics,
    productShowEpics,
    productFormEpics,
);
