import { combineEpics } from 'redux-observable';
import productEpics from './product';
import sessionEpics from './session';

export default combineEpics(
    productEpics,
    sessionEpics,
);
