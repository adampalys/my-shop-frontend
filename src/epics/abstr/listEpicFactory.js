import { from } from 'rxjs';
import {
    debounceTime, switchMap, map, takeUntil,
} from 'rxjs/operators';
import { combineEpics, ofType } from 'redux-observable';
import { fetchJsonCollection } from '../../service/ajax';

const prepareItems = items => (
    items.map(item => ({
        ...item,
    }))
);

export const createEpics = (listActions) => {
    const loadCollectionType = listActions.loadCollection().type;

    const loadCollectionEpic = action$ => (
        action$.pipe(
            ofType(loadCollectionType),
            debounceTime(250),
            switchMap(({ endpoint }) => (
                from(fetchJsonCollection(endpoint))
                    .pipe(
                        map(prepareItems),
                        takeUntil(action$.ofType(loadCollectionType)),
                    )
            )),
            map(items => listActions.updateAllItems(items)),
        )
    );

    return combineEpics(
        loadCollectionEpic,
    );
};

export default {
    createEpics,
};
