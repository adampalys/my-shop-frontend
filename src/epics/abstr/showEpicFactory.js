import { from } from 'rxjs';
import {
    debounceTime, switchMap, map, takeUntil,
} from 'rxjs/operators';
import { combineEpics, ofType } from 'redux-observable';
import { fetchJson } from '../../service/ajax';

const prepareData = data => ({
    ...data,
});

export const createEpics = (showActions) => {
    const loadDataType = showActions.loadData().type;

    const loadDataEpic = action$ => (
        action$.pipe(
            ofType(loadDataType),
            debounceTime(250),
            switchMap(({ endpoint, id }) => (
                from(fetchJson(`${endpoint}/${id}`))
                    .pipe(
                        map(prepareData),
                        takeUntil(action$.ofType(loadDataType)),
                    )
            )),
            map(data => showActions.updateData(data)),
        )
    );

    return combineEpics(
        loadDataEpic,
    );
};

export default {
    createEpics,
};
