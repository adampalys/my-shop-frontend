import React from 'react';
import PropTypes from 'prop-types';

const colors = {
    orange: 'warning',
    blue: 'info',
};

const Label = ({ color, pullRight, children }) => (
    <span className={`label label-${colors[color]} ${pullRight ? 'pull-right' : ''}`}>
        {children}
    </span>
);

Label.propTypes = {
    color: PropTypes.string.isRequired,
    pullRight: PropTypes.bool,
    children: PropTypes.string.isRequired,
};

Label.defaultProps = {
    pullRight: false,
};

export default Label;
