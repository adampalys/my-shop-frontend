import React from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import sessionActions from '../actions/session';

const Nav = ({ isLogged, resetSession, history }) => {
    const logout = () => {
        resetSession();
        history.push('/products');
    };
    return (
        <div className="container">
            <div className="row">
                <div className="col-sm-12">

                    <nav className={`navbar navbar-${isLogged ? 'inverse' : 'default'}`}>
                        <div className="container-fluid">
                            <div className="navbar-header">
                                <button
                                    type="button"
                                    className="navbar-toggle collapsed"
                                    data-toggle="collapse"
                                    data-target="#bs-example-navbar-collapse-1"
                                >
                                    <span className="sr-only">Toggle navigation</span>
                                    <span className="icon-bar" />
                                    <span className="icon-bar" />
                                    <span className="icon-bar" />
                                </button>
                                <Link className="navbar-brand" to="/home">My Shop Application</Link>
                            </div>

                            <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul className="nav navbar-nav">
                                    <li>
                                        <Link id="productsNav" to="/products">Produkty</Link>
                                    </li>
                                </ul>
                                <ul className="nav navbar-nav navbar-right">
                                    <li className="disabled">
                                        <Link to="/cart">Koszyk</Link>
                                    </li>
                                    <li className="disabled">
                                        <Link to="/orders">Zamówienia</Link>
                                    </li>
                                    {isLogged
                                        ? (
                                            <li>
                                                <a id="logout" onClick={logout}>Wyloguj</a>
                                            </li>
                                        )
                                        : (
                                            <li>
                                                <Link id="login" to="/login">Logowanie</Link>
                                            </li>
                                        )
                                    }
                                </ul>
                            </div>
                        </div>
                    </nav>

                </div>
            </div>
        </div>
    );
};


Nav.propTypes = {
    isLogged: PropTypes.bool.isRequired,
    resetSession: PropTypes.func.isRequired,
};

const connectState = state => ({
    isLogged: state.session.isLogged,
});

const connectDispatch = dispatch => ({
    resetSession: () => dispatch(sessionActions.reset()),
});

export default connect(
    connectState,
    connectDispatch,
)(withRouter(Nav));
