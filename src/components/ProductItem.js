import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Paper from './Paper';
import Label from './Label';

const ProductItem = ({
    id, name, price, isLogged
}) => {
    return (
        <Paper>
            <h2>
                <Label color="orange" pullRight>
                    {`${price} zł`}
                </Label>
            </h2>
            <h2>
                <Link className="productName" to={`/products/${id}`} >
                    {name}
                </Link>
            </h2>
            <p>&nbsp;</p>
            <Link to={`/products/${id}`} className="btn btn-info">
                Szczegóły
            </Link>
            <button className="left-sm-m btn btn-primary">
                Dodaj do koszyka
            </button>

            { isLogged && (
                <Link to={`/products/edit/${id}`} className="left-sm-m btn btn-success editBtn">Edytuj</Link>
            )}
        </Paper>
    );
};

ProductItem.propTypes = {
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    isLogged: PropTypes.bool.isRequired,
};


const connectState = state => ({
    isLogged: state.session.isLogged,
});

export default connect(
    connectState,
)(ProductItem);
