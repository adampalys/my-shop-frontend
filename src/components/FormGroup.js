import React from 'react';
import PropTypes from 'prop-types';


const Label = ({
    label, name, onChange, placeholder, type, value
}) => (
    <div className="form-group" ng-class="{'has-error' : vm.errors.login}">
        <label htmlFor={name}>{label}</label>
        <input
            name={name}
            type={type}
            onChange={onChange}
            value={value}
            className="form-control"
            placeholder={placeholder}
        />
    </div>
);

Label.propTypes = {
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    type: PropTypes.string,
};

Label.defaultProps = {
    type: 'text',
};

export default Label;
