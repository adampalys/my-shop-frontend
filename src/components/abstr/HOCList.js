import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

export const connectList = ({ actions, getSelector }) => (Component) => {
    class HOCList extends React.Component {
        componentDidMount() {
            const { loadCollection } = this.props;

            loadCollection();
        }

        componentWillUnmount() {
            const { reset } = this.props;

            reset();
        }

        render() {
            return <Component {...this.props} />;
        }
    }

    const mapStateToProps = state => ({
        items: getSelector(state).items,
    });
    const mapDispatchToProps = dispatch => ({
        loadCollection: () => dispatch(
            actions.loadCollection(),
        ),
        reset: () => dispatch(
            actions.reset(),
        ),
    });

    HOCList.propTypes = {
        loadCollection: PropTypes.func.isRequired,
        reset: PropTypes.func.isRequired,
    };

    return connect(
        mapStateToProps,
        mapDispatchToProps,
    )(HOCList);
};

export default {
    connectList,
};
