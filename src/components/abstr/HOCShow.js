import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

export const connectShow = ({ actions, getSelector }) => (Component) => {
    class HOCShow extends React.Component {
        componentDidMount() {
            const { loadData, match: { params } } = this.props;

            if (params.id) {
                loadData(params.id);
            }
        }

        componentWillUnmount() {
            const { reset } = this.props;

            reset();
        }

        render() {
            return <Component {...this.props} />;
        }
    }

    HOCShow.propTypes = {
        loadData: PropTypes.func.isRequired,
        reset: PropTypes.func.isRequired,
        params: PropTypes.object,
    };

    HOCShow.defaultProps = {
        params: {},
    };

    const mapStateToProps = state => ({
        data: getSelector(state).data,
    });
    const mapDispatchToProps = dispatch => ({
        loadData: id => dispatch(
            actions.loadData(id),
        ),
        reset: () => dispatch(
            actions.reset(),
        ),
    });

    return connect(
        mapStateToProps,
        mapDispatchToProps,
    )(HOCShow);
};

export default {
    connectShow,
};
