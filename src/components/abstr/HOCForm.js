import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

export const connectForm = ({ actions, getSelector }) => (Component) => {
    class HOCForm extends React.Component {
        componentDidMount() {
            const { loadData, match: { params } } = this.props;

            if (params.id !== '0') {
                loadData(params.id);
            }
        }

        componentWillUnmount() {
            const { reset } = this.props;

            reset();
        }

        render() {
            return <Component {...this.props} />;
        }
    }

    HOCForm.propTypes = {
        loadData: PropTypes.func.isRequired,
        reset: PropTypes.func.isRequired,
        params: PropTypes.object,
    };

    HOCForm.defaultProps = {
        params: {},
    };

    const mapStateToProps = state => ({
        data: getSelector(state).data,
    });
    const mapDispatchToProps = dispatch => ({
        loadData: id => dispatch(
            actions.loadData(id),
        ),
        reset: () => dispatch(
            actions.reset(),
        ),
    });

    return connect(
        mapStateToProps,
        mapDispatchToProps,
    )(HOCForm);
};

export default {
    connectForm,
};
