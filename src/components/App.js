import React from 'react';
import { Provider } from 'react-redux';
import {
    BrowserRouter as Router,
    Route,
    Redirect,
    Switch,
} from 'react-router-dom';
import { store } from '../config/store';

import Nav from './Nav';
import ProductList from '../pages/products/ProductList';
import ProductDetail from '../pages/products/ProductDetail';
import LoginPage from '../pages/login/Page';
import ProductForm from '../pages/products/ProductForm';

const App = () => (
    <Provider store={store}>
        <Router>
        <>
            <Nav />
            <Switch>
                <Route exact path="/products" component={ProductList} />
                <Route path="/login" component={LoginPage} />
                <Route path="/products/edit/:id" component={ProductForm} />
                <Route path="/products/:id" component={ProductDetail} />
                <Redirect to="/products" />
            </Switch>
        </>
        </Router>
    </Provider>
);

export default App;
