import React from 'react';
import PropTypes from 'prop-types';

const Paper = ({ children, headerText }) => (
    <div className="panel panel-default">
        {headerText && (
            <div className="panel-heading">
                <h3 className="panel-title"><strong>{headerText}</strong></h3>
            </div>
        )}
        <div className="panel-body">
            {children}
        </div>
    </div>
);

Paper.propTypes = {
    children: PropTypes.node.isRequired,
    headerText: PropTypes.string,
};

Paper.defaultProps = {
    headerText: '',
};

export default Paper;
