import { createStore, applyMiddleware } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import thunk from 'redux-thunk';
import rootReducer from '../reducers/root';
import rootEpic from '../epics/root';

const epicMiddleware = createEpicMiddleware();

export const store = createStore(
    rootReducer,
    applyMiddleware(thunk, epicMiddleware),
);

epicMiddleware.run(rootEpic);

window.store = store;

export default {
    store,
};
