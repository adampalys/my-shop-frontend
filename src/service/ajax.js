const fetchOpt = {
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
    },
};

export const fetchJson = (endpoint, customOpt) => (
    fetch(endpoint, { ...fetchOpt, ...customOpt })
        .then((response) => {
            if (response.status >= 200 && response.status < 400) {
                return response;
            }

            const error = new Error('Data fetch error.');
            error.response = response;

            throw error;
        })
        .then(result => result.json())
);

export const fetchJsonCollection = endpoint => (
    fetchJson(endpoint)
        .then(data => data)
);

export const sendData = (endpoint, data) => (
    fetchJson(endpoint, { method: 'POST', body: JSON.stringify(data) })
        .then(response => response)
);

export default {
    fetchJson,
    fetchJsonCollection,
    sendData,
};
