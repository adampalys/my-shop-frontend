export const createActions = ({ prefix, endpoint }) => {
    const reset = () => ({
        type: `${prefix}RESET`,
    });

    const updateData = data => ({
        type: `${prefix}UPDATE_DATA`,
        data,
    });

    const loadData = id => ({
        type: `${prefix}LOAD_DATA_EPIC`,
        id,
        endpoint,
    });

    return {
        reset,
        updateData,
        loadData,
    };
};

export default {
    createActions,
};
