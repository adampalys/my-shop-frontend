export const createActions = ({ prefix, endpoint }) => {
    const reset = () => ({
        type: `${prefix}RESET`,
    });

    const updateAllItems = items => ({
        type: `${prefix}UPDATE_ALL_ITEMS`,
        items,
    });

    const loadCollection = () => ({
        type: `${prefix}LOAD_COLLECTION_EPIC`,
        endpoint,
    });

    return {
        reset,
        updateAllItems,
        loadCollection,
    };
};

export default {
    createActions,
};
