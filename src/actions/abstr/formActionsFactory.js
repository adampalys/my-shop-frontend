export const createActions = ({ prefix, endpoint }) => {
    const reset = () => ({
        type: `${prefix}RESET`,
    });

    const updateData = data => ({
        type: `${prefix}UPDATE_DATA`,
        data,
    });

    const loadData = id => ({
        type: `${prefix}LOAD_DATA_EPIC`,
        id,
        endpoint,
    });

    const setValue = (name, value) => ({
        type: `${prefix}SET_VALUE`,
        name,
        value,
    });

    return {
        reset,
        updateData,
        loadData,
        setValue,
    };
};

export default {
    createActions,
};
