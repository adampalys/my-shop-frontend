import { expect } from 'chai';
import {
    setUserData,
    showWarning,
    authenticate,
    reset,
} from './session';

describe('actions/session', () => {
    describe('#setUserData(data)', () => {
        it('should return object with type = "SET_USER_DATA" and passed data', () => {
            expect(setUserData(123)).to.be.deep.equal({
                type: 'SET_USER_DATA',
                data: 123,
            })
        });
    });
    describe('#showWarning()', () => {
        it('should return object with type = "SHOW_WARNING"', () => {
            expect(showWarning()).to.be.deep.equal({
                type: 'SHOW_WARNING',
            })
        });
    });
    describe('#authenticate(username, password)', () => {
        it('should return object with type = "AUTHENTICATE" and passed username and password', () => {
            expect(authenticate('Jan', 'Kowalski')).to.be.deep.equal({
                type: 'AUTHENTICATE',
                username: 'Jan',
                password: 'Kowalski',
            })
        });
    });
    describe('#reset()', () => {
        it('should return object with type = "SESSION_RESET"', () => {
            expect(reset()).to.be.deep.equal({
                type: 'SESSION_RESET',
            })
        });
    });
});