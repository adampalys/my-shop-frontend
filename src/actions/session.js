export const setUserData = data => ({
    type: 'SET_USER_DATA',
    data,
});

export const showWarning = () => ({
    type: 'SHOW_WARNING',
});

export const authenticate = (username, password) => ({
    type: 'AUTHENTICATE',
    username,
    password,
});

export const reset = () => ({
    type: 'SESSION_RESET',
});

export default {
    setUserData,
    showWarning,
    authenticate,
    reset,
};
