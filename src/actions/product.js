import { createActions as createListActions } from './abstr/listActionsFactory';
import { createActions as createShowActions } from './abstr/showActionsFactory';
import { createActions as createFormActions } from './abstr/formActionsFactory';
import { sendData } from '../service/ajax';

export const productListActions = createListActions({
    prefix: 'PRODUCT_LIST_',
    endpoint: '/products',
});

export const productShowActions = createShowActions({
    prefix: 'PRODUCT_SHOW_',
    endpoint: '/products',
});

export const productFormActions = createFormActions({
    prefix: 'PRODUCT_FORM_',
    endpoint: '/products',
});

export default {
    ...productListActions,
    ...productShowActions,
    ...productFormActions,
};
