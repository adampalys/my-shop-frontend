import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from '../../components/Paper';
import FormGroup from '../../components/FormGroup';
import { authenticate } from '../../actions/session';
import { getSessionSelector } from '../../reducers/session';

export class LoginPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
        };

        this.setValue = this.setValue.bind(this);
        this.login = this.login.bind(this);
    }

    setValue({ target }) {
        this.setState({
            [target.name]: target.value,
        });
    }

    login() {
        const { auth } = this.props;
        const { username, password } = this.state;

        auth(username, password);
        this.setState({
            username: '',
            password: '',
        });
    }


    render() {
        const { username, password } = this.state;
        const { showWarning } = this.props;

        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm-6 center-block">
                        <Paper headerText="Logowanie:">
                            {showWarning && <div className="alert alert-danger">Nieprawidłowy login lub hasło</div>}
                            <FormGroup
                                label="Login"
                                name="username"
                                placeholder="Twój login"
                                value={username}
                                onChange={this.setValue}
                            />
                            <FormGroup
                                label="Hasło"
                                name="password"
                                placeholder="Twoje hasło"
                                value={password}
                                onChange={this.setValue}
                                type="password"
                            />
                            <p>&nbsp;</p>
                            <button onClick={this.login} className="btn btn-primary btn-block"> Zaloguj się</button>
                        </Paper>
                    </div>
                </div>
            </div>
        );
    }
}

LoginPage.propTypes = {
    showWarning: PropTypes.bool.isRequired,
    auth: PropTypes.func.isRequired,
};

const connectState = state => ({
    showWarning: getSessionSelector(state).showWarning,
});

const connectDispatch = dispatch => ({
    auth: (username, password) => dispatch(
        authenticate(username, password),
    ),
});

export default connect(
    connectState,
    connectDispatch,
)(LoginPage);
