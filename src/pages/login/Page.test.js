import { expect } from 'chai';
import sinon from 'sinon';
import { LoginPage } from './Page';
import React from 'react';

describe('pages/login/Page', () => {
    describe('LoginPage', () => {
        let instance
        beforeEach(() => {
            instance = new LoginPage();
        });
        it('should be instance of React.Component', () => {
            expect(instance).to.be.an.instanceof(React.Component)
        });
        describe('#setValue({target})', () => {
            let event;
            let setStateStub;
            beforeEach(() => {
                event = {
                    target: {
                        name: 'test',
                        value: 123,
                    }
                }
                setStateStub = sinon.stub(instance, 'setState');
            });
            afterEach(() => {
                setStateStub.restore();
            });
            it(`should call this.setState() passing object with 
            key equal target.name and value equal target.value`, () => {
                instance.setValue(event);
                expect(
                    setStateStub.calledWith({ test: 123 })
                ).to.be.equal(true)
            });
        });
        describe('#login({target})', () => {
            let authStub;
            let setStateStub;
            beforeEach(() => {
                setStateStub = sinon.stub(instance, 'setState');
                authStub = sinon.stub();

                instance.props = {
                    auth: authStub,
                }
                instance.state = {
                    username: 'John',
                    password: 'Doe',
                }
            });
            afterEach(() => {
                setStateStub.restore();
            });
            it(`should call auth() from props 
            passing username and password from state`, () => {
                instance.login();
                expect(
                    authStub.calledWith('John', 'Doe')
                ).to.be.equal(true)
            });
            it(`should call this.setState() with object containing 
            username and pasword equal empty string`, () => {
                instance.login();
                expect(
                    setStateStub.calledWith({
                        username: '',
                        password: '',
                    })
                ).to.be.equal(true)
            });
        });
    });
});