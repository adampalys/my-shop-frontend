import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ProductItem from '../../components/ProductItem';
import { connectList } from '../../components/abstr/HOCList';
import { productListActions } from '../../actions/product';
import { getProductListSelector } from '../../reducers/product';

const filterSearch = search => item => {
    if (!item.name) {
        return true;
    }

    return item.name.toLowerCase().indexOf(search.toLowerCase()) !== -1;
}

class ProductList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            search: '',
        };

        this.setSearch = this.setSearch.bind(this);
    }

    setSearch(event) {
        this.setState({
            search: event.target.value,
        });
    }

    render() {
        const { search } = this.state;
        const { items, isLogged } = this.props;
        return (
            <div className="container">
                <div className="row">
                    <div className="col-xs-6">
                        <input
                            value={search}
                            onChange={this.setSearch}
                            type="text"
                            className="form-control"
                            placeholder="szukaj..."
                        />
                        <br />
                    </div>
                    { isLogged && (
                        <div className="col-xs-6">
                            <Link id="addBtn" to="/products/edit/0" className="btn btn-success">
                                Dodaj
                            </Link>
                        </div>
                    )}
                </div>
                <div className="row">
                    <div className="col-sm-12">
                        {[...items].reverse().filter(filterSearch(search)).map(item => (
                            <ProductItem
                                key={item.id}
                                id={item.id}
                                name={item.name}
                                price={item.price}
                            />
                        ))}
                    </div>
                </div>
            </div>
        );
    }
}

ProductList.propTypes = {
    items: PropTypes.array.isRequired,
};

const connectState = state => ({
    isLogged: state.session.isLogged,
});

const Connected = connect(
    connectState,
)(ProductList);

export default connectList({
    actions: productListActions,
    getSelector: getProductListSelector,
})(Connected);
