import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Paper from '../../components/Paper';
import Label from '../../components/Label';
import { connectShow } from '../../components/abstr/HOCShow';
import { productShowActions } from '../../actions/product';
import { getProductShowSelector } from '../../reducers/product';

const ProductDetail = ({ data }) => {
    const getCategories = () => (
        data.categories
            ? data.categories.join(', ')
            : ''
    );

    return (
        <div className="container">
            <div className="row">
                <div className="col-sm-12">
                    <Paper>
                        <h2>
                            {data.name}
                            <Label color="orange" pullRight>
                                {`${data.price} zł`}
                            </Label>
                        </h2>
                        <h3>
                            <Label color="blue">
                                {getCategories()}
                            </Label>
                        </h3>
                        <br />
                        <p>{data.description}</p>
                        <br />
                        <button className="btn btn-primary">
                            Dodaj do koszyka
                        </button>
                        <Link to="/products" className="left-sm-m btn btn-default">
                            Powrót
                        </Link>
                    </Paper>
                </div>
            </div>
        </div>
    );
};

ProductDetail.propTypes = {
    data: PropTypes.object.isRequired,
};

export default connectShow({
    actions: productShowActions,
    getSelector: getProductShowSelector,
})(ProductDetail);
