import React from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connectForm } from '../../components/abstr/HOCForm';
import { productFormActions } from '../../actions/product';
import { getProductFormSelector } from '../../reducers/product';
import { fetchJson } from '../../service/ajax';

const prepareData = data => ({
    ...data,
    categories: [],
    price: Number(data.price) || 0,
});

class ProductForm extends React.Component {
    constructor(props) {
        super(props);

        this.setValue = this.setValue.bind(this);
        this.saveForm = this.saveForm.bind(this);
        this.removeItem = this.removeItem.bind(this);
    }

    componentDidMount() {
        const { history, isLogged } = this.props;

        if (!isLogged) {
            history.push('/products');
        }
    }

    setValue({ target }) {
        const { setValue } = this.props;

        setValue(target.name, target.value);
    }

    saveForm() {
        const { data, history, match: { params } } = this.props;
        const url = params.id !== '0'
            ? `/products/${data.id}`
            : '/products';
        const method = params.id === '0'
            ? 'POST'
            : 'PUT';

        const preparedData = prepareData(data);

        fetchJson(url, { method, body: JSON.stringify(preparedData) })
            .then(() => {
                history.push('/products');
            });
    }

    removeItem() {
        const { data, history } = this.props;

        fetchJson(`/products/${data.id}`, { method: 'DELETE' })
            .then(() => {
                history.push('/products');
            });
    }

    render() {
        const {
            data,
        } = this.props;

        return (
            <div className="container">

                <div className="row">
                    <div className="col-sm-6">
                        <div className="form-group">
                            <label htmlFor="name">Nazwa produktu</label>
                            <input onChange={this.setValue} value={data.name} name="name" type="text" className="form-control" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="description">Opis</label>
                            <textarea onChange={this.setValue} value={data.description} name="description" rows="5" className="form-control" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="price">Cena w zł</label>
                            <input onChange={this.setValue} value={data.price || 0} name="price" type="number" className="form-control" />
                        </div>
                    </div>

                    <div className="col-sm-6">
                        <h2>{data.name}</h2>
                        <p>{data.description}</p>
                        <h3>
                            <span className="label label-warning">
                                {`${data.price || 0} zł`}
                            </span>
                        </h3>
                    </div>
                </div>

                <div className="row">
                    <div className="col-sm-12">
                        <hr />
                        <button id="saveBtn" onClick={this.saveForm} type="submit" className="btn btn-primary">Zapisz zmiany</button>
                        <Link id="cancelBtn" to="/products" className="left-sm-m btn btn-default">
                            Anuluj
                        </Link>
                        {data.id !== '0' && (
                            <button id="removeBtn" onClick={this.removeItem} className="left-sm-m btn btn-danger">
                                Usuń
                            </button>
                        )}
                        <p>&nbsp;</p>
                    </div>
                </div>

            </div>
        );
    }
}

ProductForm.propTypes = {
    setValue: PropTypes.func.isRequired,
};

const connectState = state => ({
    isLogged: state.session.isLogged,
});

const connectDispatch = dispatch => ({
    setValue: (name, value) => dispatch(productFormActions.setValue(name, value)),
});

const Connected = connect(
    connectState,
    connectDispatch,
)(withRouter(ProductForm));

export default connectForm({
    actions: productFormActions,
    getSelector: getProductFormSelector,
})(Connected);
