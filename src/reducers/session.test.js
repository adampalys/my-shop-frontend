import { expect } from 'chai';
import reducer, {
    getSessionSelector,
    initState,
} from './session';

describe('/reducers/session', () => {
    describe('#getSessionSelector(state)', () => {
        let state;
        beforeEach(() => {
            state = {
                session: 123
            }
        });
        it('should return session from passed state object', () => {
            const result = getSessionSelector(state)
            expect(result).to.be.equal(123)
        });
    });
    describe('#reducer(state, action)', () => {
        let initStateClone;
        let state;
        let action;
        beforeEach(() => {
            initStateClone = {...initState}
            state = {
                test: 123,
            }
            action = {}
        });
        describe('action.type is equal "SET_USER_DATA"', () => {
            beforeEach(() => {
                action.type = 'SET_USER_DATA'
                action.data = {
                    firstName: 'firstName',
                    lastName: 'lastName',
                    username: 'username',
                    token: 'token',
                }
            });
            it(`should return object with firstName, lastName, username,
            token from passed action.data and with showWarning equal false 
            and isLogged equal true`, () => {
                const result = reducer(state, action);
                expect(result).to.be.deep.equal({
                    firstName: 'firstName',
                    lastName: 'lastName',
                    username: 'username',
                    token: 'token',
                    showWarning: false,
                    isLogged: true,
                })
            });
        });
        describe('action.type is equal "SET_USER_DATA"', () => {
            beforeEach(() => {
                action.type = 'SHOW_WARNING'
            });
            it('should return clone of initState with showWarning equal true', () => {
                const result = reducer(state, action);
                initStateClone.showWarning = true;
                expect(result).to.be.deep.equal(initStateClone)
            });
        });
        describe('action.type is equal "SESSION_RESET"', () => {
            beforeEach(() => {
                action.type = 'SESSION_RESET';
            });
            it('should return clone of initState ', () => {
                const result = reducer(state, action);
                expect(result).to.be.deep.equal(initStateClone)
            });
        });
        describe('called with diffrent action.type', () => {
            beforeEach(() => {
                action.type = 'TEST123'
            });
            it('should return passed state', () => {
                const result = reducer(state, action);
                expect(result).to.be.equal(state)
            });
        });
    });
});