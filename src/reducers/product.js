import { combineReducers } from 'redux';
import { createReducer as createListReducer } from './abstr/listReducerFactory';
import { createReducer as createShowReducer } from './abstr/showReducerFactory';
import { createReducer as createFormReducer } from './abstr/formReducerFactory';

const productListReducer = createListReducer('PRODUCT_LIST_');
const productShowReducer = createShowReducer('PRODUCT_SHOW_');
const productFormReducer = createFormReducer('PRODUCT_FORM_');

export const getProductListSelector = state => state.product.list;
export const getProductShowSelector = state => state.product.show;
export const getProductFormSelector = state => state.product.form;

export default combineReducers({
    list: productListReducer,
    show: productShowReducer,
    form: productFormReducer,
});
