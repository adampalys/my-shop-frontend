export const initState = {
    firstName: '',
    lastName: '',
    username: '',
    token: '',
    showWarning: false,
    isLogged: false,
};

export const getSessionSelector = state => state.session;

const reducer = (state = initState, action) => {
    const type = action.type ? action.type : null;

    switch (type) {
    case 'SET_USER_DATA':
        return {
            firstName: action.data.firstName,
            lastName: action.data.lastName,
            username: action.data.username,
            token: action.data.token,
            showWarning: false,
            isLogged: true,
        };
    case 'SHOW_WARNING':
        return { ...initState, showWarning: true };
    case 'SESSION_RESET':
        return { ...initState };
    default:
        return state;
    }
};

export default reducer;
