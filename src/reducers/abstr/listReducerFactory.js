const initState = {
    items: [],
};

export const createReducer = (prefix) => {
    const reducer = (state = initState, action) => {
        const type = action.type ? action.type : null;

        switch (type) {
        case `${prefix}UPDATE_ALL_ITEMS`:
            return {
                ...state,
                items: [...action.items],
            };
        case `${prefix}RESET`:
            return { ...initState };
        default:
            return state;
        }
    };

    return reducer;
};

export default {
    createReducer,
};
