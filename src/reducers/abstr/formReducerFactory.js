const initState = {
    data: {},
};

export const createReducer = (prefix) => {
    const reducer = (state = initState, action) => {
        const type = action.type ? action.type : null;

        switch (type) {
        case `${prefix}UPDATE_DATA`:
            return {
                ...state,
                data: { ...action.data },
            };
        case `${prefix}RESET`:
            return { ...initState };
        case `${prefix}SET_VALUE`:
            return { ...state, data: { ...state.data, [action.name]: action.value } };
        default:
            return state;
        }
    };

    return reducer;
};

export default {
    createReducer,
};
